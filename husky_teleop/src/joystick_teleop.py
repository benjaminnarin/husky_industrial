#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
import sys

class JoystickTeleop:
    def __init__(self, cmd_topic, joystick_topic, speed=1.0, go_button=3):
        self.speed = speed
        self.go_button = go_button
        self.twist_pub = rospy.Publisher(cmd_topic, Twist, queue_size=1)
        rospy.init_node('joystick_teleop',anonymous=True)
        joystick_sub = rospy.Subscriber(joystick_topic, Joy, self.joystick_callback)

    def joystick_callback(self, data):
        go_button_pressed = data.buttons[self.go_button]

        if go_button_pressed:
            twist = Twist()
            twist.linear.x = self.speed
            twist.angular.z = 0.0
            twist.linear.y = 0.0
            self.twist_pub.publish(twist)


def main():
    cmd_topic = rospy.get_param('cmd_topic')
    joystick_topic = rospy.get_param('joystick_topic')
    speed = float(rospy.get_param('speed'))
    go_button = int(rospy.get_param('go_button'))

    JoystickTeleop(cmd_topic, joystick_topic, speed, go_button)
    rospy.spin()
    

if __name__ == '__main__':
    main()