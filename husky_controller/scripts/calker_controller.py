#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Joy
from std_msgs.msg import Bool, Int32
import sys

class calker_controller:
	def __init__(self):
		joystick_sub = rospy.Subscriber('/joy',Joy, self.joystick_callback)
		self.calker_pub = rospy.Publisher('/calker_msg', Bool, queue_size=1)
		self.linear_pub = rospy.Publisher('/linear_msg', Int32, queue_size=1)
		self.calker_msg = Bool()
		self.linear_msg = Int32()
	def joystick_callback(self,joy):
		# Bool calker_msg
		# Int32 linear_msg
		if (joy.buttons[5] == 1):
			self.calker_msg.data = True
			self.calker_pub.publish(self.calker_msg)
		elif (joy.buttons[5] == 0):
			self.calker_msg.data = False
			self.calker_pub.publish(self.calker_msg)
		if(joy.axes[1] < 0):
			self.linear_msg.data = 1
			self.linear_pub.publish(self.linear_msg)
		elif(joy.axes[1] > 0):
			self.linear_msg.data = 127
			self.linear_pub.publish(self.linear_msg)
		else:
			self.linear_msg.data = 64
			self.linear_pub.publish(self.linear_msg)


if __name__ == '__main__':
	rospy.init_node('calker_controller',log_level=rospy.DEBUG)
	calker_controller = calker_controller()
	rospy.spin()
