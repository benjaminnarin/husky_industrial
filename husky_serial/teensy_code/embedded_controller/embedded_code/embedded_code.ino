//#include <RoboClaw.h>
//#include <BMSerial.h>
#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Bool.h>
#define HWSERIAL Serial1
const int ledPin = 13;
const int calkerPin = 23;

int lin_speed = 0;

ros::NodeHandle nh;
//Basic Micro Robo Claw Simple Serial Test
//Switch settings: SW2=ON and SW5=ON
//Make sure Arduino and Robo Claw share common GND!
//#include “BMSerial.h”
//Pin 23 for trigger pin
//mySerial(recive,trasmit)

//BMSerial rclaw(4,3,1000);

void calker_callback( const std_msgs::Bool& calker_msg){
  if (calker_msg.data == true){
    digitalWrite(calkerPin, HIGH);
  }
  else if(calker_msg.data == false){
    digitalWrite(calkerPin, LOW);
  }
}

void linear_callback( const std_msgs::Int32& linear_msg){
 lin_speed = linear_msg.data;
//HWSERIAL.write(linear_msg.data);
//Serial.print("In Callback");
}

ros::Subscriber<std_msgs::Bool> sub1("calker_msg", &calker_callback );
ros::Subscriber<std_msgs::Int32> sub2("linear_msg", &linear_callback );

void setup() {
 nh.initNode();
// Serial.begin(9600);
 HWSERIAL.begin(38400);
 pinMode(ledPin, OUTPUT);
 pinMode(calkerPin, OUTPUT);
 nh.subscribe(sub1);
 nh.subscribe(sub2);
}

void loop() {
//  Serial.println(lin_speed, DEC);
  HWSERIAL.write(lin_speed);
  nh.spinOnce();
  delay(500);
}
